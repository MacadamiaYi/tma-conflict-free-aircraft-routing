# TMA conflict-free aircraft routing

In order to provide a visual representation of the efficiency of our proposed methods and to
better understand how our results compare to historical data, we have included a demo video.
This video serves to illustrate the effectiveness of our approach and offers a tangible
demonstration of the improvements achieved through our method. The two videos are
“1_Historical_Result.mkv” and “2_MILP_Result.mkv”.

Upon reviewing the video, notable differences between the historical trajectory and our
results become apparent. The historical trajectory exhibits multiple instances of vectoring and
holding circles, whereas our method demonstrates aircraft trajectories that strictly adhere to the
designated STAR routes, without any holding patterns. Furthermore, the landing time of the
last aircraft in our method is observed to be earlier compared to the historical data, indicating
improved efficiency in our approach.


To demonstrate the completion of holding operation, we also provide the demo video of the case
when holding operation is triggered from our optimizer. The demo video includes both
departure and arrival aircraft, called “3_Holding_Case_Arrival_Departure.mp4”.


## Repository structure
File "1_Historical_Result.mkv" gives the simulation for historical results.

File "2_MILP_Result.mkv" gives the simulation for our optimized results under MILP method.

File "3_Holding_Case_Arrival_Departure.mp4" gives the simulation for the case when holding operation is triggered in our optimizer.